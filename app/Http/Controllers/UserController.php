<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //

    public function findByMobile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobile' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }


        $user = User::where('mobile', $request->input('mobile'))->first();

        if(!$user){
            return response()->json(['user not found'], 404);
        }

        return $user->toArray();
    }


    public function registerToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'push_device_token' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }

        $user = User::find($request->input('userId'));

        if(! $user){
            return response()->json(['User not found'], 404);
        }

        $user->update([
            'push_device_token' => $request->input('push_device_token')
        ]);

        return $user->toArray();
    }
}
