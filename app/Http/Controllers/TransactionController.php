<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Validator;


class TransactionController extends Controller
{
    const STEP_ONE = 1;
    const STEP_TWO = 2;
    const STEP_THREE = 3;

    /**
     * @param $id
     * @return mixed
     */
    public function retrieve($id){
        return Transaction::where('id', $id)->with(['payer', 'payee'])->first();
    }

    public function establish(Request $request){
        $validator = Validator::make($request->all(), [
            'transaction_id' => 'required',
            'payee_mid_number' => 'required',
            'payee_mid_calc_time' => 'required',
            'payee_shared_calc_time' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }

        $transaction = Transaction::find($request->input('transaction_id'));
        $transaction->payee_mid_number = $request->input('payee_mid_number');
        $transaction->payee_mid_calc_time = $request->input('payee_mid_calc_time');
        $transaction->payee_shared_calc_time = $request->input('payee_shared_calc_time');
        $transaction->save();

        $payer = User::where('id', $transaction->payer_id)->first();

        FCMController::sendPushMessage(
            new Request(            [
                "to" => $payer->push_device_token,
                "notification" => [
                    "body" => "Establishing Secure Channel",
                    "title" => "Pace Payment",
                    "icon" => "ic_launcher"
                ],
                "data" => [
                    "transaction_id" => $request->input('transaction_id'),
                    "step" => self::STEP_TWO,
                    "transaction" => $transaction->toArray()
                ]
            ])
        );
        return response()->json($transaction);
    }

    public function initiate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payee_id' => 'required',
            'payer_mid_number' => 'required',
            'payer_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }


        $data = $request->all();
        $data['status'] = 'PENDING';
        return Transaction::create($data);
    }


    public function notifyUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payee_id' => 'required',
            'transaction_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }

        $user = User::where('id', $request->input('payee_id'))->first();

        return redirect()->action(
            'FCMController@sendPushMessage', [
                "to" => $user->push_device_token,
                "notification" => [
                    "body" => "Initiated Secure Channel",
                    "title" => "Pace Payment",
                    "icon" => "ic_launcher"
                ],
                "data" => [
                    "transaction_id" => $request->input('transaction_id'),
                    "step" => self::STEP_ONE
                ]
            ]
        );

    }



    public function sendPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'transaction_id' => 'required'
        ]);


        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }

        $amount = $request->input('amount');
        $transaction = Transaction::find($request->input('transaction_id'));
        $payer = User::where('id', $transaction->payer_id)->first();
        $payee = User::where('id', $transaction->payee_id)->first();

        FCMController::sendPushMessage(
            new Request(            [
                "to" => $payee->push_device_token,
                "notification" => [
                    "body" => "You have received $ {$amount} from {$payer->mobile}",
                    "title" => "Pace Payment",
                    "icon" => "ic_launcher"
                ],
                "data" => [
                    "transaction_id" => $request->input('transaction_id'),
                    "step" => self::STEP_THREE,
                    "transaction" => $transaction->toArray()
                ]
            ])
        );
    }
}
