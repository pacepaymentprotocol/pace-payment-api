<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FCMController extends Controller
{

    /**
     * Example Parameter $data = array('from'=>'Lhe.io','title'=>'FCM Push Notifications');
     * $target = 'single token id or topic name';
     * or
     * $target = array('token1','token2','...'); // up to 1000 in one request for group sending
     *
     * @param Request $request
     * @return mixed
     * @internal param eg $data . data, to or registration_ids
     * @internal param $target
     */
    public static function sendPushMessage(Request $request){

        $validator = Validator::make($request->all() , [
            'data' => 'required',
            'to' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }
        $data = json_encode($request->all());
        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key = 'AAAAL0bxXeY:APA91bHCqAdknjHisAxgA-J8NacKpj8jdP3gyFfQQqESTaMKuWalMqqghrHffvz2GCVxVqD_zJMnERD3Mzd29rzZPVVHJbq-SM8ZS7TogYhdwczF3_M0npe86SOFPjzZLNhLAS6xFbU7';
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$server_key
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}
