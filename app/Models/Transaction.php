<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = 'transaction';

    protected $guarded = ['id'];



    public function payer()
    {
        return $this->hasOne(
          User::class,
            'id',
            'payer_id'
        );
    }


    public function payee()
    {
        return $this->hasOne(
            User::class,
            'id',
            'payee_id'
        );
    }
}
