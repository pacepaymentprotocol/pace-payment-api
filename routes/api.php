<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'auth'], function () {
    Route::any('register', 'Auth\\RegisterController@register');
    Route::any('password/reset', 'Auth\\ResetPasswordController@reset');
    Route::any('login', 'Auth\\LoginController@login');
});


Route::group(['prefix' => 'user'], function () {
    Route::any('find-by-mobile', 'UserController@findByMobile');
    Route::any('register-token', 'UserController@registerToken');
});


Route::group(['prefix' => 'push'], function () {
    Route::any('send-notification', 'FCMController@sendPushMessage');
});



Route::group(['prefix' => 'transaction'], function () {
    Route::any('initiate', 'TransactionController@initiate');
    Route::any('notify', 'TransactionController@notifyUser');
    Route::any('retrieve/{id}', 'TransactionController@retrieve');
    Route::any('establish', 'TransactionController@establish');
    Route::any('send-payment', 'TransactionController@sendPayment');
});