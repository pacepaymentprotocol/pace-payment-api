<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payer_id');
            $table->string('payer_mid_number');
            $table->string('payer_mid_calc_time');
            $table->string('payer_shared_calc_time');
            $table->string('payee_id');
            $table->string('payee_mid_number');
            $table->string('payee_mid_calc_time');
            $table->string('payee_shared_calc_time');
            $table->string('amount');
            $table->string('protocol');
            $table->string('shared');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
