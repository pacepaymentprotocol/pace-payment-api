<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'ahmed',
            'email' => 'ahmedaoumar@gmail.com',
            'mobile' => '6467052738',
            'password' => bcrypt('secret'),
        ]);
    }
}
